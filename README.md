
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>A2 Milk Chennai, Organic Milk, Cow Milk, Nattu Kozhi Muttai, Paneer</title>
        
        <meta name="description" content="Buy Organic Milk Chennai, Aranpaal Provides Fresh Cow Milk And Pure A2 Milk, Nattu Kozhi, Paneer Online in Chennai"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" type="image/png" href="http://www.aranpaal.com/images/favicon.png"/>
		<meta name="google-site-verification" content="lMd6havm5QW9eXuJuu3Bz5WDrKFCpL2IbtnySeyW9Sw" />
        <!--Bootstrap 4-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
        <!--icons-->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />
        <link rel="stylesheet" href="css/style.css" />
         <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
         <style>
            .tooltip > span {
   text-align: justify;
 }
        </style>
   <meta name="google-site-verification" content="EWfhyCbjvCH3TTMppBUOkxR32EVAx4T-8ADOzJYVLZ0" />
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-149409518-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-149409518-1');
</script>

	</head>
    <body>
        <!--header-->
        <nav class="navbar navbar-toggleable-md navbar-inverse fixed-top sticky-navigation">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="ion-grid icon-sm"></span>
            </button>
            <a class="navbar-brand hero-heading" href="#" style="width: 40%;"><img src="images/AranLogo2.png" style="width:20%;"></a>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item mr-3">
                        <a class="nav-link page-scroll" href="#main">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item mr-3">
                        <a class="nav-link page-scroll" href="#features">About Us</a>
                    </li>
                   <li class="nav-item mr-3">
                        <a class="nav-link page-scroll" href="#vision">Our Vision</a>
                    </li>
                    <li class="nav-item mr-3">
                        <a class="nav-link page-scroll" href="#farm">Our Farm</a>
                    </li>
                   <!--<li class="nav-item mr-3">
                        <a class="nav-link page-scroll" href="#pricing">Locations</a>
                    </li>--->
                    <li class="nav-item mr-3">
                        <a class="nav-link page-scroll" href="#product">Products</a>
                    </li>
                    <li class="nav-item mr-3">
                        <a class="nav-link page-scroll" href="#faq">FAQ'S</a>
                    </li>
                    <li class="nav-item mr-3">
                        <a class="nav-link page-scroll" href="#team">Subscription</a>
                    </li>
                    
                    <li class="nav-item mr-3">
                        <a class="nav-link page-scroll" href="#contact">Contact Us</a>
                    </li>
                </ul>
            </div>
        </nav>

        <!--main section-->
        <section class="bg-texture hero" id="main" style="min-height:100%;
  background:url(images/bg.png);
  background-size:cover;background-size: 107% 119%;
    background-repeat: no-repeat;">
            <div class="container">
                <div class="row d-md-flex brand">
                    
                    <div class="col-md-12 col-sm-12 text-white wow fadeInRight" style="margin-top:-9%;">
                        <center><h2 class="pt-4">Aran’s Traditional Milk</h2>
                       
                      <div class="col-md-2 col-sm-4"></div>
                      <div class="col-md-8 col-sm-4">
                        <p class="mt-5">
                            Aran’s traditional milk is pure A2 milk, hand-milked in a traditional way from healthy native
Indian breeds and reaches at your doorstep.
                        </p>
                        </div>
                        <div class="col-md-2 col-sm-4"></div>
                        
                        
                        <p class="mt-5" style="margin-top: 1rem!important;">
                            
                            <!--<a style="background:#fff;" href="images/brochure.jpg" class="btn btn-white mb-2 page-scroll"><span style="color:#000;">Download Brochure</span></a>--!>
                        </p></center>
                    </div>
                </div>
            </div>
        </section>

 


        <!--features-->
        
        <section class="bg-alt" id="features">
            <div class="container">
                <div class="row d-md-flex mt-5">
                    
  <div class="col-md-12 col-sm-8  col-xs-12 text-center">
                        <h2 class="text-primary" style="font-family: 'Rubik', sans-serif;">About Us</h2>
                        <hr>
                        <p class="lead pt-3">
                           
                        </p>
                    </div>
  
 <br/> <br/>
 
 
 <div class="col-sm-4">
      
      <div class="card" style="width: 100%;">
           
            <div class="card-body" style="padding: 3%;">
              <h5 class="card-title" style="text-align: center;font-size: 26px;">Milking Process</h5>
              <p class="card-text">The milking is done from indigenous cows by using hands. No machines are used in order to ensure no harm is done to the cows</p>
              
            </div>
      </div>
 </div>
 <div class="col-sm-4">
     <div class="card"  style="width: 100%;">
            
            <div class="card-body" style="padding: 3%;">
              <h5 class="card-title" style="text-align: center;font-size: 26px;">Packing Methods</h5>
              <p class="card-text" style="text-align: justify;">As soon as milking is done, the milk is filtered and packed in the FSSAI certified place with hairnets and gloves on this packing is done into the 50 microns wrappers which are not reactive to the food items. Again, no machines are used for packing to contribute to the environment, as they consume more water and power.

</p>
              
            </div>
      </div>
     
 </div>
 <div class="col-sm-4">
     <div class="card"  style="width: 100%;">
           
            <div class="card-body" style="padding: 3%;">
              <h5 class="card-title" style="text-align: center;font-size: 26px;">Milk Delivery</h5>
              <p class="card-text">As soon as packing and quality check are done, the milk packets are collected and brought for delivery.</p>
              
            </div>
      </div>
     
     
 </div>
 

     <div class="col-sm-12">
         <h4 style="text-align: center;">Payment Methods</h4>
     </div>
     <div class="col-sm-4">
         <center><img src="images/payment(1).png" alt="A2 Milk Chennai" title="A2 Milk Chennai">
         <p>Cash</p></center>
     </div>
     <div class="col-sm-4">
         <center><img src="images/credit-cards.png" alt="A2 Milk Chennai" title="A2 Milk Chennai">
         <p>Net Banking,Visa/Debt Cards</p></center>
     </div>
     <div class="col-sm-4">
         <center><img src="images/sudexo2.png"  alt="A2 Milk Chennai" title="A2 Milk Chennai">
         <p>MEAL/SODEXO CARDS </p></center>
     </div>
 

                </div>
            </div>
        </section>
        <section class="bg-alt" id="vision">
            <div class="container">
                  <div class="col-md-12 col-sm-8  col-xs-12 text-center">
                        <h2 class="text-primary" style="font-family: 'Rubik', sans-serif;">Our Vision</h2>
                        <hr>
                        <p class="lead pt-3">
                           
                        </p>
                    </div>
                               
                      <p style="font-size: 19px;text-align: center;">To serve every person with chemical-free, healthy and natural food by following re-engineered traditional methods and also working towards nourishing our culture and language.</p>
 
          </div>
</section>
<section class="bg-alt" id="farm">
            <div class="container">
                  <div class="col-md-12 col-sm-8  col-xs-12 text-center">
                        <h2 class="text-primary" style="font-family: 'Rubik', sans-serif;">Our Farm</h2>
                        <hr>
                        <p class="lead pt-3">
                           
                        </p>
                    </div>        
                     <div class="container text-center my-3">
    
    <div class="row mx-auto my-auto">
        <div id="recipeCarousel" class="carousel slide w-100" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <img class="d-block col-md-4 col-xs-12 img-fluid" src="farm_images/DSC_0125.JPG" alt="Nattu Kozhi Muttai Chennai" title="Nattu Kozhi Muttai Chennai"  style="width: 100%;height: 177px;">
                </div>
                <div class="carousel-item">
                    <img class="d-block col-md-4 col-xs-12  img-fluid" src="farm_images/DSC_0129.JPG" alt="Cow Milk Chennai" title="Cow Milk Chennai"  style="width: 100%;height: 177px;">
                </div>
                <div class="carousel-item">
                    <img class="d-block col-md-4 col-xs-12  img-fluid" src="farm_images/DSC_0135.JPG" alt="A2 Milk Chennai" title="A2 Milk Chennai" style="width: 100%;height: 177px;">
                </div>
                <div class="carousel-item">
                    <img class="d-block col-md-4 col-xs-12  img-fluid" src="farm_images/DSC_0140.JPG" alt="Organic Milk Chennai" title="Organic Milk Chennai"  style="width: 100%;height: 177px;">
                </div>
                <div class="carousel-item">
                    <img class="d-block col-md-4 col-xs-12  img-fluid" src="farm_images/DSC_0144.JPG" alt="Naatu Maatu Paal Chennai" title="Naatu Maatu Paal Chennai"  style="width: 100%;height: 177px;">
                </div>
                <div class="carousel-item">
                    <img class="d-block col-md-4 col-xs-12  img-fluid" src="farm_images/DSC_0146.JPG" alt="Naatu Maatu Paal Chennai" title="Naatu Maatu Paal Chennai" style="width: 100%;height: 177px;">
                </div>
                <div class="carousel-item">
                    <img class="d-block col-md-4 col-xs-12  img-fluid" src="farm_images/DSC_0153.JPG" title="A2 Milk Chennai" alt="A2 Milk Chennai"  style="width: 100%;height: 177px;">
                </div>
                <div class="carousel-item">
                    <img class="d-block col-md-4 col-xs-12  img-fluid" src="farm_images/DSC_0159.JPG" alt="A2 Milk Chennai" title="A2 Milk Chennai"  style="width: 100%;height: 177px;">
                </div>
               <div class="carousel-item">
                    <img class="d-block col-md-4 col-xs-12  img-fluid" src="farm_images/DSC_0171.JPG" alt="Cow Milk Chennai" title="Cow Milk Chennai"  style="width: 100%;height: 177px;">
                </div>
            </div>
            <a class="carousel-control-prev" href="#recipeCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#recipeCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    
</div>




 
          </div>
</section>
        <!--pricing-->
    <!--    <section class="bg-faded" id="pricing">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-8  col-xs-12 text-center">
                        <h2 class="text-primary" style="font-family: 'Rubik', sans-serif;">Locations</h2>
                        <hr>
                    </div>
                </div>
                <div class="row" style="margin-top: 3%;">
                    
                        <div class="col-xs-6 col-sm-6 col-md-3" style="margin-bottom:4%;"><em class="ion-ios-location-outline icon-md" style="font-size:30px;"></em> <span class="location">Egattoor</span></div>
                        <div class="col-xs-6 col-sm-6 col-md-3" style="margin-bottom:4%;"><em class="ion-ios-location-outline icon-md" style="font-size:30px;"></em> <span class="location">Kazhipattur</span></div>
                        <div class="col-xs-6 col-sm-6 col-md-3" style="margin-bottom:4%;"><em class="ion-ios-location-outline icon-md" style="font-size:30px;"></em> <span class="location">Kelambakkam</span></div>
                        <div class="col-xs-6 col-sm-6 col-md-3" style="margin-bottom:4%;"><em class="ion-ios-location-outline icon-md" style="font-size:30px;"></em> <span class="location">Navalur</span></div>
                        <div class="col-xs-6 col-sm-6 col-md-3" style="margin-bottom:4%;"><em class="ion-ios-location-outline icon-md" style="font-size:30px;"></em> <span class="location">Padur</span></div>
                        <div class="col-xs-6 col-sm-6 col-md-3" style="margin-bottom:4%;"><em class="ion-ios-location-outline icon-md" style="font-size:30px;"></em> <span class="location">Pudupakkam</span></div>
                        <div class="col-xs-6 col-sm-6 col-md-3" style="margin-bottom:4%;"><em class="ion-ios-location-outline icon-md" style="font-size:30px;"></em> <span class="location">Siruseri</span></div>
                        
                    
                </div>
                <div class="price-table">
		    <div class="container">
			    <div class="row">
                    <div class="col-sm-3">
                        
                        
                    </div>
                    <div class="col-sm-3"></div>
                    <div class="col-sm-3"></div>
                    <div class="col-sm-3"></div>
				</div>
			</div>
</div>
            </div>
        </section>-->

        <!--download-->
        
        <section class="bg-faded" id="product">
            <div class="container">
                <div class="row" style="margin-bottom: 8%;">
                    <div class="col-md-12 col-sm-8 col-xs-12 text-center">
                        <h2 class="text-primary" style="font-family: 'Rubik', sans-serif;">Our Products</h2>
                        <hr>
                    </div>
                </div>
                <div class="row rowimages">
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <center>
                        <img src="images/milk_products/Amilk_350ml.png" alt="350ml" style="width: 50%;    margin-bottom: 8%;">
                        
                        <h4 style="font-size: 22px;">350ml-Rs.28</h4>
						</center>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <center>
                        <img src="images/milk_products/Amilk_500ml.png" alt="500ml" style="width: 55%;    margin-bottom: 8%;">
                        
                        <h4 style="font-size: 22px;">500ml-Rs.37</h4>
                        </center>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <center>
                        <img src="images/milk_products/Amilk_1000ml.png" alt="1000ml" style="width: 54%;     margin-bottom: 8%;">
                        
                        <h4 style="font-size: 22px;">1000ml-Rs.74</h4>
                        </center>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <center>
                        <img src="images/milk_products/Aran-Naatu-Kozhi-Muttai-image-v2.png" alt="Each-Rs.12" style="width: 90%;     margin-bottom: 8%;">
                        
                        <h4 style="font-size: 22px;">Each-Rs.12</h4>
                        </center>
                    </div>					
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <center>
                        <img src="images/milk_products/Paneer-1.png" alt="200 gms" style="width: 100%;    margin-bottom: 8%;">
                        
                        <h4 style="font-size: 22px;">Paneer 200 gms - Rs.130</h4>
                        </center>
                    </div>
				</div>
                
            </div>
            
        </section>  
        
        <section class="bg-faded" id="faq">
            
            <div class="container">
                
                <div class="row" style="margin-bottom: 8%;">
                    <div class="col-md-12 col-sm-8  col-xs-12 text-center">
                        <h2 class="text-primary" style="font-family: 'Rubik', sans-serif;">FAQ'S</h2>
                        <hr>
                    </div>
                </div>
                
                
 <div id="accordion">
  <div class="card">
    <div class="card-header" id="headingOne" >
		<a class="" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
      <h5 class="mb-0">
        <!--<a class="" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">-->
          Why A2 milk ?
        
      </h5>
	</a>		
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body" style="padding: 2%;">
        It is healthier, easy to digest, improves immunity & develops strength, promotes mental growth, reduces tiredness and above all it is as good as a mother’s milk.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
		<a class="collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
      <h5 class="mb-0">
        
          Which cows produce A2 Milk?
        
      </h5>
			</a>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body" style="padding: 2%;">
        All native Indian breeds like Gir, Red Sindhi, Sahiwal, Kangayam, Puliakulam etc are some of the A2 cow breeds.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
		<a class="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
      <h5 class="mb-0">
        
          What are the cow breeds we have ?
        
      </h5>
			</a>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body" style="padding: 2%;">
        Our cow breed includes mainly Red Sindhi, Puliakulam, kanchi kuttai
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
        <a class="collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
		<h5 class="mb-0">
         So, what do we feed them ?
        </h5>
			</a>
    </div>
    <div id="collapseFour" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body" style="padding: 2%;">
       Our cows graze on green pasture in a free environment and find their own food along which they are also fed with hay & cultivated fodder.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <a class="collapsed" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
		<h5 class="mb-0">
        
          Where do we bring the milk from ?
        
      </h5>
		  </a>
    </div>
    <div id="collapse5" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body" style="padding: 2%;">
       We bring milk from the farmers who use these cows for agricultural purposes in the outskirts of Chennai.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
		<a class="collapsed" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
      <h5 class="mb-0">
        
          How do we trust its a2 milk ?
        
      </h5>
			</a>
    </div>
    <div id="collapse6" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body" style="padding: 2%;">
        Well, we can arrange a field visit and show the cows from which we milk, the environment and their habitat* 
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
		<a class="collapsed" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">
      <h5 class="mb-0">
        
         How much does the milk cost ?
        
      </h5>
			</a>
    </div>
    <div id="collapse7" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body" style="padding: 2%;">
         <ol>
            <li>350 ml  – Rs 27</li>
            <li>500 ml  – Rs 36</li>
            <li>1000 ml – Rs 72</li>    
         </ol>     
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
		<a class="collapsed" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8">
      <h5 class="mb-0">
        
          Why is the milk costly ?
        
      </h5>
</a>
	  </div>
    <div id="collapse8" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body" style="padding: 2%;">
       In general, on an average, Indian/Desi breed cows can produce only around 2-2.5 liters of milk per time in a day.Cows can produce more milk only when it’s injected with extra growth hormone, fed with grain-based diets (instead of grass) and exposing cows to longer periods of artificial light. <br/>
       We believe and go by the natural way hoping to bring back the traditional milk which causes no harm to people, cows and the environment.

      </div>
    </div>
  </div>
   <div class="card">
    <div class="card-header" id="headingTwo">
		  <a class="collapsed" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapse9">
      <h5 class="mb-0">
      
        Do you add Formalin to your Milk ?
        
      </h5>
			  </a>
    </div>
    <div id="collapse9" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body" style="padding: 2%;">
       Strictly NO. Formalin is used to keep the milk from spoiling sooner which often ends up reacting with Protein molecules in the consumers’ stomach and causes huge troubles with digestion. This is completely against our policy of serving natural food and hence nothing is added to the milk. 
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
		  <a class="collapsed" data-toggle="collapse" data-target="#collapse15" aria-expanded="false" aria-controls="collapse15">
      <h5 class="mb-0">
      
         Is our Milk Pasteurized ?
        
      </h5>
			  </a>
    </div>
    <div id="collapse15" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body" style="padding: 2%;">
        Our milk is not processed in order to preserve real minerals and vitamins. They are just raw and contains no additives.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
		<a class="collapsed" data-toggle="collapse" data-target="#collapse10" aria-expanded="false" aria-controls="collapseTwo">
      <h5 class="mb-0">
        
          How do we deliver ?
        
      </h5>
			</a>
    </div>
    <div id="collapse10" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body" style="padding: 2%;">
        Our milk comes directly from the farm, packed and delivered at your doorstep. And so you can have a nutritious milk for your refreshment in the morning 
      </div>
    </div>
  </div>
    
  <div class="card">
    <div class="card-header" id="headingTwo">
		<a class="collapsed" data-toggle="collapse" data-target="#collapse12" aria-expanded="false" aria-controls="collapseTwo">
      <h5 class="mb-0">
        
          Wanna try Aran? Call/Whatsapp us.
        
      </h5>
			</a>
    </div>
    <div id="collapse12" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body" style="padding: 2%;">
        9003538198<br/>
        9080501610

      </div>
    </div>
  </div>
 
  
</div>
                
                
                
            </div>
        </section>    

        <!--team-->
        <section class="bg-alt" id="team">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-8  col-xs-12  text-center">
                        <h2 class="text-primary" style="font-family: 'Rubik', sans-serif;">Subscription</h2>
                        <hr>
                    </div>
                </div>
                <div class="row d-md-flex mt-5">
                    <div class="col-md-4 col-xs-12">
                        <div class="boxstyle">
                        <h5 class="text-center">Instructions</h5>
                        <hr>
                        <p>Raw Milk Prices</p>
<p>350 ml-Rs.27</p>
<p>500 ml-Rs.36</p>
<p>1000 ml-Rs.72</p>
<p>Paneer 1 Pack (200 gms) Rs.130</p>
<p>** For COD No Extra Charges</p>
                      </div>  
                    </div>
                    <div class="col-md-8 col-xs-12">
                    <div class="row boxstyle" id="subform">    
                     <div class=" col-md-6">
                        <h5 class="text-center">Order Details</h5>
                        <hr>
                        <p>Daily Delivery Available</p>
                        
                        
                        <select class="form-control mygap" id="location">
                            <option value="">Select Location</option>
                                                              <option value="1">Egattoor</option> 
                                                                <option value="2">Kazhipattur</option> 
                                                                <option value="3">Kelambakkam</option> 
                                                                <option value="4">Navalur</option> 
                                                                <option value="5">Padur</option> 
                                                                <option value="6">Pudupakkam</option> 
                                                                <option value="7">Siruseri</option> 
                                                                <option value="8">Sholinganallur</option> 
                                                                <option value="9">Medavakkam</option> 
                                                                <option value="10">Perumbakkam</option> 
                                                                <option value="11">Kumaran Nagar</option> 
                                                                <option value="12">Thambaram</option> 
                                                                <option value="14">Chromepet</option> 
                                                                <option value="15">Nanganallur</option> 
                                                                <option value="16">Guindy</option> 
                                                                <option value="17">Velachery</option> 
                                                                <option value="18">Pallikaranai</option> 
                                                                <option value="19">Thiruvanmiyur</option> 
                                                                <option value="20">Adyar</option> 
                                                                <option value="21">Thoraipakkam</option> 
                                                                <option value="23">OMR</option> 
                                                                <option value="24">ECR</option> 
                              							<option value="22">Others[Chennai]</option>
                        </select>
                       
                       
                        
                         <input class="form-control mygap" id="datepicker1"  type="text" value="13-10-2019" readonly />
                         <input  type="hidden"  id="datepicker"/>

                        <div class="mygap row">
                           
                            <div class="col-md-6 col-xs-12">
                                <input type="checkbox" class="" id="pro1"> 350 ml
                                
                            </div>
                            <div class="col-md-6 mygap">
                                  <select class="form-control" id="quan1">
                                    <option value="">Select Packets</option>
                                     <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                            
                                 </select>
                                
                            </div>
                            
                            <div class="col-md-6">
                                <input type="checkbox" class="" id="pro2"> 500 ml
                               
                            </div>
                            <div class="col-md-6 mygap">
                                  <select class="form-control" id="quan2">
                                    <option value="">Select Packets</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                 </select>
                                 
                            </div>
                            
                            <div class="col-md-6">
                                <input type="checkbox" class="" id="pro3"> 1000 ml
                                 
                            </div>
                            <div class="col-md-6 mygap">
                                  <select class="form-control" id="quan3">
                                    <option value="">Select Packets</option>
                                     <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                            
                                 </select>
                            </div>
                            
                            <div class="col-md-6">
                                <input type="checkbox" class="" id="pro4"> Paneer 200 gms
                            </div>
                            <div class="col-md-6 mygap">
                                  <select class="form-control" id="quan4">
                                    <option value="">Select Packets</option>
                                     <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                            
                                 </select>
                            </div>
                            
                            
                        </div>
                        
                          
                       </div> 
                     <div class="col-md-6"> 
                        <h5 class="text-center">Delivery Details</h5>
                        <hr>
                        <p>Sample Product Available</p>
                        <input type="text" class="form-control mygap" placeholder="Full Name" id="fname">
						   
			<input type="text" class="form-control mygap" placeholder="Email Address" id="email_id">   
                        
                        <input type="text" class="form-control mygap" placeholder="Number" id="mobnumber" onkeypress="return event.charCode >= 48 &amp;&amp; event.charCode <= 57" maxlength="10">
                        
                        <textarea class="form-control mygap" placeholder="Address" rows="6" id="address"></textarea>
                        
                        
                     </div> 
                     <div class="col-md-6">
                         <textarea class="form-control mygap"  placeholder="Order Instructions" rows="2" id="order_inst"></textarea>
                         <!--<input type="radio" class="mygap"> Cash on Delivery (COD)<br/>
                         <input type="radio" class="mygap"> Credit / Debit Cards (Coming Soon)<br/>
                         <input type="radio" class="mygap"> Net Banking  (Coming Soon)<br/>-->
                     </div>
                     <div class="col-md-6">
                        <button  type="button" class="btn btn-success btn-block" style="height: 56px;margin-top: 0%;box-shadow:none;" id="fsubmit">Submit</button>
                     </div>
                     
                     </div>   
                    </div>
                </div>
            </div>
<a href="https://web.whatsapp.com/send?phone=919003538198&text=Vanakkam *Aran!* %0A %0A
I would like to subscribe for prepaid one-time one-week trial. Below are my address and map location. %0A %0A
*Name :* %0A
*Quantity :* %0A
*Address :* %0A
*Map Location :* $$ To be Shared $$" class="float" target="_blank">
<i class="fa fa-whatsapp my-float"></i>
</a>        </section>
        <div id="snackbar"></div>
       

        <!--contact-->
        <section class="bg-texture-collage p-0" id="contact">
            <div class="container">
                <div class="row d-md-flex text-white text-center wow fadeIn" style="padding: 13px 0px;">
                    <div class="col-sm-4">
                        <p><em class="ion-ios-telephone-outline icon-md"></em></p>
                        <p class="lead">9003538198 <br/> 9080501610</p>
                    </div>
                    <div class="col-sm-4">
                        <p><em class="ion-ios-email-outline icon-md"></em></p>
                        <p class="lead"><span style="color:#fff;cursor:pointer;">aran.iyalangadi@gmail.com</span></p>
                    
                        <p class="social">
                        <a href="https://www.facebook.com/aranpaal/" target="_blank" style="color:#fff;cursor:pointer;"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                        <i class="fa fa-twitter" aria-hidden="true"></i>
                       <!-- <i class="fa fa-google-plus-official" aria-hidden="true"></i>-->
                        <i class="fa fa-instagram" aria-hidden="true"></i>
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <p><em class="ion-ios-location-outline icon-md"></em></p>
                        <p class="lead">Chennai, India</p>
                    </div>
                </div>
            </div>
        </section>

        <!--footer-->
        <section class="bg-footer" id="connect" style="padding: 9px 0px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-xs-12  wow fadeIn" style="padding: 13px 0px;">
                      <div class="row">
                        <div class="col-md-4" style="text-align:center;"><span style="color:#fff;cursor:pointer;" data-toggle="modal" data-target="#terms">Terms & Conditions</span></div>
                        <div class="col-md-4" style="text-align:center;"><span style="color:#fff;cursor:pointer;" data-toggle="modal" data-target="#privacy">Privacy Policy</span></div>
                        <div class="col-md-4" style="text-align:center;"><span style="color:#fff;cursor:pointer;" data-toggle="modal" data-target="#refund">Refund Policy</span></div>
						  
						  
<!-- The Term Modal -->
  <div class="modal fade" id="terms">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Terms & Conditions</h4>
          <button type="button" class="close" data-dismiss="modal" style="font-size: 60px;">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body" style="height: 390px;overflow: auto;">
			
			<p>These are the terms and conditions for the purchase of milk listed on the website aranmilk.com. Please read the following terms and conditions very carefully as your use of " ARAN MILK" website https://aranmilk.com and good and services offered are subject to your acceptance of and compliance with the following terms and conditions. By using ARAN Milk services you agree to be bound by the following terms and conditions of use. The term "our" or "we" or "us" are references to "ARAN Milk"; The term "you" refers to the user or viewer of our Site</p>
			
			<p>ARAN Milk may update these terms and conditions at any time. You should check from time to time to review these terms and conditions to ensure that you are aware of any changes. Using or accessing this website indicates your acceptance of these terms and conditions. If you do not wish to accept these terms and conditions you should not place an order and should not continue to use this website.</p>
			
			
			<h5>USER ACCOUNT AND REGISTRATION TERMS</h5>
			
			<p>To register with ARAN Milk via this website you must be 18 years of age or older. Registering at “ARAN Milk” is free service and you will receive a username and password upon completing the website's registration process. You are responsible for maintaining the confidentiality of the username and password, and are fully responsible for all activities that occur under your account</p>
			
			<p>“ARAN Milk” cannot and will not be liable for any loss or damage arising from your failure to keep your password secure. You agree that if you provide any information that is untrue, inaccurate, not current or incomplete or we have reasonable grounds to suspect that such information is untrue, inaccurate, not current or incomplete, or not in accordance with the Terms of Use, we have the right to indefinitely suspend or terminate or block access of your membership with “ARAN Milk” and refuse to provide you with access to the website.</p>
			
			<h5>ORDERING AND PAYMENT</h5>
			
			<p>Any order that you place with us is subject to product availability, delivery capacity and acceptance by us. When you place your order online, we will send you an e-mail to confirm that we have received it. Order will only be confirmed for delivery once we have received your payment. You must inform us immediately if any details are incorrect. </p>
			
			<p>For new users or subscriptions, by default a week subscription will be taken as order. Later which you can choose the subscription of your choice.</p>
			
			<p>You can pay for the milk by any of the following manner:</p>
			
			<ul>
				<li>Cash</li>
				<li>Cheque</li>
				<li>Debit/Credit cards</li>
				<li>Meal cards</li>
				
			</ul>
			
			<p>In case of any payment failure, the amount will be refunded to your account within 10 business days. </p>
			
			<p>You agree not to hold us responsible for banking charges incurred due to payments made on your account. We reserve the right to suspend your membership and any further deliveries and take any other action as we consider appropriate in the event of a failed payment or us being refused authority for payment.</p>
			
			<p>We will take all reasonable care, to keep the details of your order and payment secure, but in the absence of negligence on our part we cannot be held liable for any loss you may suffer if a third party procures unauthorized access to any data you provide when accessing or ordering from “ARAN Milk”.</p>
			
			<p>Milk purchased from “ARAN Milk” are intended for your use only and are not for resale and that you are acting as principal only and not as agent for another party when receiving the Services.</p>
			
			<h5>DELIVERY</h5>
			
			<p>Delivery will be made to the address specified by you when you register on the Website and is free of charge. We reserve the right to restrict or cease deliveries in any area at our sole discretion. Delivery time will be provided once the order is confirmed, usually before 8. We reserve the right to alter your time of delivery and your milkman at its discretion. We will always make every effort to inform this change in advance. </p>
			
			<h5>PRICE</h5>
			
			<p>The price of the product will be as quoted on the website at the time you place your order, subject only to any inadvertent technical error for which we will not be liable. In case the prices are higher or lower on the date of delivery no additional charges will be collected or refunded as the case may be at the time of the delivery of the order.</p>
			
			<h5>CANCELLATIONS</h5>
			
			<p>If you would like to end or pause your subscription, you can do so anytime through your online account. In such a case the amount corresponding to the paused dates will be refunded to you through cash or net banking or will be compensated in the next month’s subscription which we decide is appropriate.</p>
			
			<h5>DEFECTIVE OR INCOMPLETE PRODUCT</h5>
			
			<p>In the event that the product delivered to you are incomplete or defective, you must notify us within 24 hours. We shall investigate and provide and appropriate remedy to you if applicable.</p>
			
			<h5>ACCEPTANCE OF PRIVACY POLICY</h5>
			
			<p>The User hereby consents, expresses and agrees that he has read and fully understands the “Privacy Policy” of “ARAN Milk” with respect to the website. The user further consents that the terms and contents of such a “Privacy Policy” are acceptable to him/her.
</p>
			
			<h5>TERMS AND CONDITIONS FOR THE USE OF THIS WEBSITE</h5>
			
			<p>Use of this website and our intellectual property rights. We have made this website available to you for your own personal use. We may modify, withdraw or deny access to this website at any time.</p>
			
			<p>This website and all the materials contained in it are protected by intellectual property rights, including copyright, and either belong to us or are licensed to us to use. Materials include, but are not limited to, the design, layout, look, appearance, graphics and documents on the website, as well as other content such as articles, stories and other text.</p>
			
			<p>You may not copy, redistribute, republish or otherwise make the materials on this website available to anyone else without our consent in writing.</p>
			
			<p>You may print or download materials from this website for your own personal use or copy the content to other individuals for their personal information provided that:</p>
			
			<ul>
				<li>no materials are modified in any way</li>
				<li>no graphics are used separately from accompanying text</li>
				<li>our copyright and trade mark notices appear in all copies and you acknowledge this website as the source of the material</li>
				<li>the person to whom you are providing these materials is made aware of these restrictions.</li>
			</ul>
			
			<h5>LIMITATION OF LIABILITY</h5>
			<p>This website should only be used for information purposes. It is not advice and you should not rely on it to make (or refrain from making) any decisions or take (or refrain from taking) any action.</p>
			<p>ARAN Milk does not guarantee that this website will operate free of error or that it is free from computer virus or any other contaminating computer programs</p>
			<p>This website is made available for public viewing on the basis that ARAN Milk excludes to the fullest extent lawfully permitted all liability whatsoever for any loss or damage howsoever arising out of the use of this website or reliance upon the content of this website.</p>
			<h5>Information on this website</h5>
			<p>The information contained on this website is given for general information and interest purposes only. Whilst we try and ensure the information contained on the website is accurate and up to date, we cannot be responsible for any inaccuracies in the information. As a result, you should not rely on this information, and we recommend that you take further advice or seek further guidance before taking any action based on the information contained on this website. Our liability to you as explained above remains unaffected by this.</p>
			
			<h5></h5>
			  <p>We reserve the right to:</p>
			  <p>Modify or withdraw, temporarily or permanently, the Website (or any part thereof) with or without notice to you and you confirm that we shall not be liable to you or any third party for any modification to or withdrawal of the Website; and/or</p>
			  <p>Change the Terms from time to time, and your continued use of the Website (or any part thereof) following such change shall be deemed to be your acceptance of such change. It is your responsibility to check regularly to determine whether the Terms have been changed. If you do not agree to any change to the Terms then you must immediately stop using the Website.</p>
			  <p>We may transfer our rights and obligations under a contract to another organization, but this will not affect your rights or our obligations under these Terms. We will always notify you by posting on this webpage if this happens.</p>
			
			<h5>SEVERABILITY</h5>
			<p>If any provision of the terms is determined to be invalid or unenforceable in whole or in part, such invalidity or unenforceability shall attach only to such provision or part of such provision and the remaining part of such provision and all other provisions of these terms shall continue to be in full force and effect.</p>
			<h5>INDEMNITY</h5>
			<p>You agree to indemnify and hold ARAN Milk and (as applicable) its subsidiaries, affiliates, officers, directors, agents, and employees, harmless from any claim or demand, including reasonable attorneys' fees, made by any party due to or arising out of your breach of the Terms of Use (and any documents they incorporate by reference), your own use of the Site or the Services, or your violation of any law or the rights of a third party.</p>
			<h5>Linking</h5>
			<p>We may link to other websites which are not within our control. When we do this, we will try and make it as clear as possible that you are leaving our website. We are not responsible for these websites in any way. It is your responsibility to check the terms and conditions and privacy policy on any other website which you visit.</p>
			<p>You may not link to this website from another website without our consent in writing.</p>
			
			<h5>Governing law and jurisdiction</h5>
			<p>The formation, existence, construction, performance, validity and all aspects whatsoever of these terms and conditions or of any term of these terms and conditions will be governed by the law of India.</p>
			<p>The Chennai courts will have exclusive jurisdiction to settle any disputes which may arise out of or in connection with these terms and conditions or use of the website</p>
			
			<h5>Entire Agreement</h5>
			<p>The Terms of Use and our Privacy Policy constitute the sole and entire agreement between you and ARAN Milk with respect to the Website and supersede all prior and contemporaneous understandings, agreements, representations and warranties, both written and oral, with respect to the Website</p>
			
			<h5>Your Comments and Concerns</h5>
			<p> If you have any feedback about our product you have bought or have a query regarding a delivery, please contact us at aran.iyalangadi@gmail.com</p>
			
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
  <!----------->	
						  
  <!-- The Privacy Modal -->
  <div class="modal fade" id="privacy">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Privacy Policies</h4>
          <button type="button" class="close" data-dismiss="modal" style="font-size: 60px;">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body" style="height: 390px;overflow: auto;">
			<h5>Your privacy is important to us</h5>
			
			<p>At ARAN Milk, we are committed to protecting any information that either you have submitted with us as subscriber, or we have stored as a business. In order to maintain transparency, we consider it necessary to let our users know how and for what we use information.</p>
			
			<p>By accessing our Websites or social media sites, you accept the terms of this Privacy Policy.</p>
			
			<h5>Information We Collect and How We Collect It</h5>
			
			<p>We collect information you voluntarily give us as well as information collected automatically. This includes personally identifiable information such as your name, contact number, postal address, e-mail address and financial information (when you buy products). You voluntarily provide this information when you fill in forms on our Websites, enter a contest or promotion, respond to surveys, place purchase orders.</p>
			
			<p>You also may post comments via an on-line forum, offer recipes, respond to surveys, state preferences. Please be aware that your User Contributions posted on our Website are available for the public to see.</p>
			
			<p>If you post personal information when you interact on our social media sites, depending on your privacy settings, this information may become public on the Internet.</p>
			
			<p>We are not responsible for personal information you choose to post publicly on interactive portions of the Websites or our social media sites</p>
			
			<h5>What we use your data for</h5>
			
			<p>Specifically, we may use your information to communicate with you in response to your enquiry for information or to answer your questions and to improve your experience and public information like user ratings, reviews, etc.</p>
			
			<p>We will ensure that your personal data will not be disclosed to other organizations, institutions and authorities unless required by law. We will not sell, rent or lease your personal data to others.  However you agree that if we transfer ownership or management of the Site to a third party we may also transfer your data to such third party, provided such third party agrees to observe this privacy policy.</p>
			
			<p>We will only keep information about you for as long as we need to. We have an internal retention policy and once the appropriate retention period has elapsed, the data is deleted.</p>
			
			<h5>Anonymous Information Collected Through Automatic Data Collection Technologies</h5>
			
			<p>Cookies are small text files that web sites can send to your computer. A cookie can be thought of as an internet user’s identification card, which tells a web site when the user has returned. Cookies are not computer programs, and can’t read other information saved on your hard drive. They cannot be used to disseminate viruses, or get a user’s e-mail address etc. They only contain and transfer to the web site as much information as the users themselves have disclosed to that web site.</p>
			
			<p>Cookies make the interaction between users and web sites faster and easier. Without cookies, it would be very difficult for a web site to remember the user’s preferences or registration details for a future visit. We also use cookies to monitor statistical data on our site, no identifying data is stored.</p>
			
			<p>Information on deleting or controlling cookies is available at www.AboutCookies.org. Please note that by deleting our cookies or disabling future cookies you may not be able to access certain areas or features of our site.</p>
			
			<p>By continuing to use this site, you agree to our collection of cookies.</p>
			
			<h5>Security</h5>
			
			<p>We have implemented measures designed to secure your personal information from accidental loss and from unauthorized access, use, alteration and disclosure. Unfortunately, the transmission of information via the Internet is not 100% secure. While we cannot guarantee the security of your personal information transmitted to our Websites, we will take all reasonable steps to safeguard this information once we receive it.</p>
			
			<h5>OTHER WEBSITES</h5>
			
			<p>Our website may have links to other websites. This privacy policy only applies to our website, (www.aranmilk.com) and not to those sites. We cannot be held accountable for the reliability of the service and data of the other sites and hence, we recommend reading the individual website’s privacy policy while using them.</p>
			
			<h5>Complaints</h5>
			
			<p>We are committed to working with you to obtain a fair and rapid resolution of any complaints or disputes relating to privacy matters. Please send us your questions and comments regarding our privacy policy to aran.iyalanga</p>
			
			<h5>Changes in Policies</h5>
			
			<p>From time to time we may use customer information for new, unanticipated uses not previously disclosed in our privacy notice. If our information practices change at some time in the future we will post the policy changes to our website to notify you of these changes and provide you with the ability to opt-out of these new uses. If you are concerned about how your information is used, you should check back at our website periodically.</p>
				
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
  <!----------->	
  <!-- The Refund Modal -->
  <div class="modal fade" id="refund">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Return & Refund Policy</h4>
          <button type="button" class="close" data-dismiss="modal" style="font-size: 60px;">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
			<h5>CANCELLATION</h5>
			<p>If you would like to end or pause your subscription, you can do so anytime through your online account quoting the date(s). In such a case the amount corresponding to the paused dates will be refunded to you through cash or net banking or will be compensated in the next month’s subscription which we decide is appropriate.</p>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
  <!----------->							  
						  
						  
						  
						  
						  
						  
						  
						  
                      </div>
                    </div>
                    <div class="col-md-6 col-xs-12  wow fadeIn" style="padding: 6px 0px;  font-size:smaller;">
                       
                      
                        <p class="pt-2 text-muted" style="float:right;text-align: center;">
                            &copy; 2018 Aran's Milk 
                            Proudly present
                        </p>
                    </div>
                </div>
            </div>
        </section>





        <script data-cfasync="false" src="/cdn-cgi/scripts/d07b1474/cloudflare-static/email-decode.min.js"></script><script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
         <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"></script>
       
        <script src="js/scripts.js"></script>



        
		<script>

function myFunction(data) {
     // alert(data);
    // Get the snackbar DIV
    document.getElementById("snackbar").innerHTML=data;
var x = document.getElementById("snackbar");

    // Add the "show" class to DIV
    x.className = "show";

    

    // After 3 seconds, remove the show class from DIV
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}

$( function() {
    $( "#datepicker1" ).datepicker({

dateFormat: 'dd-mm-yy',
minDate: +2

}).on("change", function() {
    var seldate=$("#datepicker1").val();
     $("#datepicker").val(seldate);
  });


  } );

		$('#recipeCarousel').carousel({
  interval: 10000
})

$('.carousel .carousel-item').each(function(){
    var next = $(this).next();
    if (!next.length) {
    next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));
    
    if (next.next().length>0) {
    next.next().children(':first-child').clone().appendTo($(this));
    }
    else {
      $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
    }
});

$("#fsubmit").click(function(){ 
     var   location            =       $("#location").val();
     var    quan1              =       $("#quan1").val();
     var    quan2              =       $("#quan2").val();
     var    quan3              =       $("#quan3").val();
     var    quan4              =       $("#quan4").val();
     var   order_inst        =       $("#order_inst").val();
     var   fname               =       $("#fname").val();
     var   email_id            =       $("#email_id").val();
     var    mobnumber   =       $("#mobnumber").val();
     var    address            =       $("#address").val();
     var    sub_date          =       $("#datepicker").val();

   if(!location){
          myFunction("Please Select Location");
          //alert("Please Select Location");
          return false;    
   }
  if(!sub_date){
          myFunction("Please Select Date of Subscription");
          //alert("");
          return false;    
   }
   if(!fname){
          myFunction("Please Add Name");
          return false;    
   }
   if(!mobnumber){
           
          myFunction("Please add Mobile Number");
          return false;
   }else{
          if(mobnumber.length !="10"){
               myFunction("Mobile Number is Not Valid");   
               return false;
          }
   }
   if(!address){
           myFunction("Please add Addrees");
          return false;
   }
    var isChecked1 = $("#pro1").is(":checked");
    var isChecked2 = $("#pro2").is(":checked");
    var isChecked3 = $("#pro3").is(":checked");
    var isChecked4 = $("#pro4").is(":checked");
    
            if (isChecked1) {
                   if(!quan1){
                    myFunction("Please Select Quantity");
                   return false;    
                       }else{
                   var pro1= 1;
                  
                   }
            } else {
                   pro1= 0;
                   quan1 = 0;
                //alert("CheckBox1 not checked.");
            }
            if (isChecked2) {
                if(!quan2){
                    myFunction("Please Select Quantity");
                   return false;    
                       }else{
                   var pro2= 1;
                  
                   }
            } else {
                   pro2 = 0;
                   quan2 = 0;
               // alert("CheckBox2 not checked.");
            }
           if (isChecked3) {
               if(!quan3){
                    myFunction("Please Select Quantity");
                   return false;    
                       }else{
                   var pro3 = 1;
                  
                   }
            } else {
                          pro3 = 0;
                          quan3 = 0;
                //alert("CheckBox3 not checked.");
            }
           if (isChecked4) {
               if(!quan4){
                    myFunction("Please Select Quantity");
                   return false;    
                       }else{
                   var pro4= 1;
                  
                   }
            } else {
                              pro4= 0;
                              quan4 = 0;
                //alert("CheckBox4 not checked.");
            }

     var n = $("input:checked").length;    
        if(n == 0) {
            myFunction("Please Select Atleast one product");
            return false;
        } else {
           // return true;
        }
    var dataString="location="+location+"&pro1="+pro1+"&pro2="+pro2+"&pro3="+pro3+"&pro4="+pro4+"&quan1="+quan1+"&quan2="+quan2+"&quan3="+quan3+"&quan4="+quan4+"&order_inst="+order_inst+"&fname="+fname+"&email_id="+email_id+"&mobnumber="+mobnumber+"&address="+address+"&sub_date="+sub_date;

 $("#fsubmit").prop('disabled', true);
//alert(dataString);

$.ajax({
					type:"POST",
					url : 'sendmail.php',
					data: dataString,
					success:function(data)
					{

					   //alert(data);
                                           if(data=='1'){
                                                       myFunction("Some error happend from server side,try again");
                                                        $("#fsubmit").prop('disabled', false);
                                           }else{
                                                  $dataarr=data.split('<####>');   
                                                  $("#successmodal").modal('show');
                                                  $("#successmodal").find('.modal-body').html($dataarr['0']);
                                                  $( "#subform" ).load( "index.php #subform");
                                                  //$("#sucess_msg").html( $dataarr['0']);
                                                   $("#fsubmit").prop('disabled', false);
                                           }

					}
				});

});

$(document).ready(function () {
  //called when key is pressed in textbox
  $("#mobnumber").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
       
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
});

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});


function reload(){
     window.location.href="index.php";
}
		</script>
    </body>
<div id="successmodal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
             <div id="sucess_msg"></div>
      </div>
      <div class="modal-footer">
        <center><button type="button" class="btn btn-default" data-dismiss="modal" onclick="reload()">Close</button></center>
      </div>
    </div>

  </div>
</div>
</html>
